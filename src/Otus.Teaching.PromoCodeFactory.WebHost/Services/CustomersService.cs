﻿using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Service;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomersService : Service.Customer.CustomerBase
    {
        private readonly IRepository<Core.Domain.PromoCodeManagement.Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Core.Domain.PromoCodeManagement.Customer> customerRepository
            , IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }


        public override async Task<CustomerShortResponseList> GetCustomers(Empty request, ServerCallContext context)
        {

            var response = new CustomerShortResponseList();

            var customers = await _customerRepository.GetAllAsync();

            response.CustomersShortResponse.AddRange(customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }));

            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerId request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                return null;
            else
            {
                var customerGrpc = new CustomerResponse()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                };

                if (customer.Preferences.Any())
                    customerGrpc.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse()
                    {
                        Id = x.PreferenceId.ToString(),
                        Name = x.Preference.Name
                    }));

                return customerGrpc;
            }
        }

        public override async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x.Id)).ToList());

            var customer = new Core.Domain.PromoCodeManagement.Customer
            {
                Id = Guid.NewGuid(),

                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.AddAsync(customer);

            return new CustomerResponse() { Id = customer.Id.ToString() };

        }

        public override async Task<CustomerResponse> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Not found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x.Id)).ToList());

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);
       
            return await GetCustomer(new CustomerId() { Id = request.Id }, context); ;
        }

        public override async Task<Empty> DeleteCustomer(CustomerId request, ServerCallContext context)
        {
        
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Not found");

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
